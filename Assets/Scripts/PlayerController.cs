﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float xp;
    public int level = 1;

    [Header("Abilities")]
    public float aimAbility = 0;
    public float reloadingAbility = 0;
    public float runningAbility = 0;


    [Header("Properties")] 
    public float playerSpread = 0.3f;
    public float reloadingSpeed = 1f;
    public float movementSpeed = 3f;


    [HideInInspector] public float playerScore;
    [HideInInspector] public float lastLevelXP;
    [HideInInspector] public float nextLevelXp; 
    [HideInInspector] public int score;
    [HideInInspector] public int skillPoints; 
    [HideInInspector] public int equipedMags = 3;


    private GameObject endRunButton;
    private GameObject shooterReadyPanel;

    private GameManager gameManager;
    private Timer timer;

    void Start()
    {
        nextLevelXp = 10f * (level / 3f);
        endRunButton = GameObject.Find("UI").transform.Find("HUD").transform.Find("EndStageButton").gameObject;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        shooterReadyPanel = GameObject.Find("UI").transform.Find("HUD").transform.Find("ShooterReady").gameObject;
        timer = GameObject.Find("Targets").transform.Find("StartPosition").GetComponent<Timer>();
    }



    private void OnTriggerEnter(Collider other)
    {
       
        if(other.tag == "StartPosition")
        {

            if (!timer.shooterReady)
            { 
            
                shooterReadyPanel.SetActive(true);

            }

        }
        
        
        if(other.tag == "LastFiringStage")
        {

            endRunButton.SetActive(true);
            

        }

        if (other.tag == "FiringStage")
        {



        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "StartPosition")
        {
            shooterReadyPanel.SetActive(false);

            if(timer.shooterReady && !timer.started)
            {
                gameManager.EarlyStart();

            }

        }


        if (other.tag == "LastFiringStage")
        {
            endRunButton.SetActive(false);
                        

        }
        
        if (other.tag == "FiringStage")
        {

        }
    }


    void Update()
    {
        LevelProgress();
    }


    void LevelProgress()
    {
         nextLevelXp = 10f * (level*level)/6 ;

        if (xp >= nextLevelXp)
        {
            level++;
            skillPoints++;
            lastLevelXP = nextLevelXp;
        }

    }

    

}
