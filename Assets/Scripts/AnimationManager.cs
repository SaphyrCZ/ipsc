﻿
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public GameObject guns;

    [HideInInspector] public Animator playerAnimator;
    [HideInInspector] public Animator gunAnimator;

    private CharacterController characterController;
    private Movement movement;
    private GameObject activeGun;
    private ButtonManager buttons;
    private RaycastHit aimHit;
    private Camera fpsCamera;
    private GameManager gameManager;


    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        characterController = GameObject.Find("Player").GetComponent<CharacterController>();
        movement = GameObject.Find("Player").GetComponent<Movement>();
        buttons = GameObject.Find("UI").GetComponent<ButtonManager>();
        fpsCamera = GameObject.Find("Player").transform.Find("Fps Camera").GetComponent<Camera>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    { 
        playerAnimator.SetFloat("move", movement.isMoving);
        Aim();

    }


    void Aim()
    {

        if (Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out aimHit, 10))
        {
            string tag = aimHit.transform.tag;


            if (tag == "Target-Steel" || tag == "Target-Paper")
            {
                if (aimHit.collider.isTrigger)
                {

                    playerAnimator.SetBool("aim", true);
                    fpsCamera.fieldOfView = Mathf.Lerp(fpsCamera.fieldOfView, 25, 10f * Time.deltaTime);

                    
                }
            }


        }
        else
        {
            fpsCamera.fieldOfView = Mathf.Lerp(fpsCamera.fieldOfView, 60, 10f * Time.deltaTime);
            playerAnimator.SetBool("aim", false);
        }

    }


    public void Reload()
    {
        string gunName = gameManager.GetActiveGun();
        Gun activeGun = GameObject.Find(gunName).GetComponent<Gun>();

        activeGun.Reload();

    }

    public void AddAmmo()
    {
        string gunName = gameManager.GetActiveGun();
        Gun activeGun = GameObject.Find(gunName).GetComponent<Gun>();

        activeGun.AddAmmo();

    }
    



    public void EndShot()
    {
        playerAnimator.SetBool("HipShot", false);
        playerAnimator.SetBool("AimedShot", false);
             
    }

    public void BrassEject()
    {
        string gunName = gameManager.GetActiveGun();
        Gun activeGun = GameObject.Find(gunName).GetComponent<Gun>();

        activeGun.BrassEject();
    }
    
    

}
