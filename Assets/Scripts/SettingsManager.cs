﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class SettingsManager : MonoBehaviour
{

    public Slider lookSensSlider;
    public Slider lookDeadZoneSlider;


    void Start()
    {
        lookSensSlider.value = PlayerPrefs.GetFloat("LookSensitivity");
        lookDeadZoneSlider.value = PlayerPrefs.GetFloat("LookDeadZone");


    }

    
    

    public void SaveAndBack()
    {
        PlayerPrefs.SetFloat("LookSensitivity", lookSensSlider.value);
        PlayerPrefs.SetFloat("LookDeadZone", lookDeadZoneSlider.value);
        SceneManager.LoadScene(0);

    }

    public void InGameSaveAndBack()
    {
        // otevírazní a zavírání oken UI
        GameObject.Find("UI").transform.Find("OptionPanel").gameObject.SetActive(false); 
        GameObject.Find("UI").transform.Find("InGameMenu").gameObject.SetActive(true);


        // Nastavení hodnot
        GameObject.Find("Player").transform.Find("Fps Camera").GetComponent<MouseLook>().sensitivity = lookSensSlider.value; 
        GameObject.Find("UI").transform.Find("HUD").transform.Find("Controlls").transform.Find("LookJoystick").GetComponent<FixedJoystick>().DeadZone = lookDeadZoneSlider.value;


        //Uložení hodnot
        PlayerPrefs.SetFloat("LookSensitivity", lookSensSlider.value);
        PlayerPrefs.SetFloat("LookDeadZone", lookDeadZoneSlider.value);
    }


}
