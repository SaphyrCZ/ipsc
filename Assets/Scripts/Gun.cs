﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{

    public GunObject scriptableGun;
    public ParticleSystem muzzleFlash;
    public ParticleSystem brassEject;
    public GameObject player;
    

    [HideInInspector] public int ammoInMag;
    [HideInInspector] public int magCapacity;
    [HideInInspector] public float gunSpread;
    [HideInInspector] public float hitForce;
    [HideInInspector] public bool started = false;

    private ParticleSystem bulletImpactWood;
    private ParticleSystem bulletImpactGround;
    private ParticleSystem bulletImpactSteel;
    private ParticleSystem bulletImpactConcrete;
    private ParticleSystem bulletImpactPaper;


    /// <summary>
    /// skripty
    /// </summary>
    
    private Movement movement;
    private PlayerController playerController;
    private GameManager gameManager;
    private ButtonManager buttons;


    private AnimationManager animationManager;
    private Animator playerAnimator;

    private Camera fpsCamera;
    private RaycastHit hit;


    void Start()
    {
        ammoInMag = scriptableGun.ammoInMag;
        magCapacity = scriptableGun.magCapacity;
        gunSpread = scriptableGun.gunSpread;
        hitForce = scriptableGun.hitForce;
        
        bulletImpactWood = scriptableGun.bulletImpactWood;
        bulletImpactGround = scriptableGun.bulletImpactGround;
        bulletImpactSteel = scriptableGun.bulletImpactSteel;
        bulletImpactConcrete = scriptableGun.bulletImpactConcrete;
        bulletImpactPaper = scriptableGun.bulletImpactPaper;


        fpsCamera = player.transform.Find("Fps Camera").GetComponent<Camera>(); // nalezení objektu fps kamery, hráče a jejich komponent.
        playerController = player.GetComponent<PlayerController>();
        movement = player.GetComponent<Movement>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        buttons = GameObject.Find("UI").GetComponent<ButtonManager>();
        playerAnimator = player.transform.Find("Fps Camera").transform.Find("FPS Hands prefab").GetComponent<Animator>();
        animationManager = player.transform.Find("Fps Camera").transform.Find("FPS Hands prefab").GetComponent<AnimationManager>();

    }

    private void FixedUpdate()
    {
        if (buttons.fire && started && ammoInMag > 0)
        {
            Shoot();
            buttons.fire = false;
        }
        
        
        if (buttons.reload && playerController.equipedMags > 0)
        {
            StartReload();
            buttons.reload = false;
        }
        
        
    }

   /// <summary>
   /// STŘELBA
   /// </summary>

    public void Shoot()
    {

        if (ammoInMag == 1 && playerAnimator.GetBool("aim") == false)
        {
            playerAnimator.SetBool("HipLastShot", true);

        }

        if (ammoInMag == 1 && playerAnimator.GetBool("aim") == true)
        {
            playerAnimator.SetBool("AimedLastShot", true);

        }


        if(playerAnimator.GetBool("aim") == false && playerAnimator.GetBool("HipLastShot") == false)  // animace
        {
        
            playerAnimator.SetBool("HipShot", true);

        }

        if(playerAnimator.GetBool("aim") == true && playerAnimator.GetBool("AimedLastShot") == false)
        {
          
            playerAnimator.SetBool("AimedShot", true);

        }

        float spread = (playerController.playerSpread + gunSpread + movement.movementSpread)/2; // rozptyl = rozptyl hráče + rozptyl zbraně + rozptyl kvůli pohybu
        
        float spreadX = Random.Range(-spread, spread);
        float spreadY = Random.Range(-spread, spread);
        float spreadZ = Random.Range(-spread, spread);
            
       
        ammoInMag -= 1; // odečtení náboje ze zásobníku
        muzzleFlash.Play();
        PlayShotSound();





        // raycast výstřel
        if (Physics.Raycast(fpsCamera.transform.position, new Vector3(fpsCamera.transform.forward.x + spreadX, fpsCamera.transform.forward.y + spreadY, fpsCamera.transform.forward.z + spreadZ), out hit, 100, -1, QueryTriggerInteraction.Ignore))
        {

            BulletImpactFX(); // efekt dopadu střely
            ScoreAndXP(); // připsání score a zkušeností


            if(hit.rigidbody != null && hit.transform.gameObject.layer == 9)  // shození/rozkývání gongu
            {
                hit.rigidbody.AddForce(-hit.normal * hitForce);
                
            }

           // Debug.Log(hit.transform.name);

        }


        if (ammoInMag < 0)
        {
            ammoInMag = 0;
        }
        
    }

    /// <summary>
    /// PŘEBITÍ
    /// </summary>

    void StartReload()
    {
        playerAnimator.SetBool("StartReloading", true);

    }

    public void Reload()
    {

        playerAnimator.SetBool("Reloading", true);
        playerAnimator.SetBool("StartReloading", false);
        playerAnimator.SetBool("HipLastShot", false);
        playerAnimator.SetBool("AimedLastShot", false);


    }

    public void AddAmmo()
    {
        playerAnimator.SetBool("Reloading", false);
        ammoInMag = magCapacity;
        playerController.equipedMags -= 1; 

    }

    /// <summary>
    /// EFEKTY ZBRANĚ 
    /// </summary>

    void BulletImpactFX()
    {
        if(hit.transform.tag == "Ground-Dirt")
        {
            Instantiate(bulletImpactGround, hit.point, Quaternion.LookRotation(hit.normal));
          
        }

        if (hit.transform.tag == "Target-Steel")
        {
            Instantiate(bulletImpactSteel, hit.point, Quaternion.LookRotation(hit.normal));

        }


        if (hit.transform.tag == "Target-Paper") 
        {
            Instantiate(bulletImpactPaper, hit.point, Quaternion.LookRotation(hit.normal));

        }

    }


    void ScoreAndXP()
    {
        

        if(hit.transform.GetComponent<TargetValues>())
        {
            int scoreValue = hit.transform.GetComponent<TargetValues>().scoreValue;
            playerController.xp += scoreValue / 100f;

            if(playerAnimator.GetBool("aim") == false)
            {
                playerController.score += scoreValue*2;
            }
            else playerController.score += scoreValue; // připsání score

        }


       // Debug.Log(player.xp);

    }


    
    public void BrassEject()
    {
        brassEject.Play();
    }

    public void PlayShotSound()
    {
        GetComponent<AudioSource>().Play();
    }

}
