﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]


public class Movement : MonoBehaviour
{
    [Header("Movement properties")]
    public float speed = 1;
    public float gravity = - 9.81f;
    public float groundDistance = 0.2f;
    public float movementSpreadMultiplier = 4;
    public Transform grounder;
    public LayerMask groundMask;
    
    [HideInInspector] public float movementSpread = 0;
    [HideInInspector] public float isMoving;
    public bool canMove = true;

    private PlayerController player;
    private CharacterController characterController;
    private Vector3 playerVelocity;
    private bool isGrounded;
    private Vector3 lastPos;
    private Joystick runJoy;


    void Start()
    {
        player = GetComponent<PlayerController>();
        characterController = GetComponent<CharacterController>();
        runJoy = GameObject.Find("UI").transform.Find("HUD").transform.Find("Controlls").transform.Find("RunJoystick").GetComponent<FixedJoystick>();
      //  playerAnimator = GetComponent<Animator>();

    }



    void Update()
    {
        if (canMove)
        {         
            Move();
        }
        

    }




    // pohyb
    private void Move()
    {
        Vector3 pos = transform.position; // uložení současné pozice

        //Je hráč na zemi?
        isGrounded = Physics.CheckSphere(grounder.position, groundDistance, groundMask);

        if (isGrounded && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        float x = runJoy.Horizontal;
        float z = runJoy.Vertical;

        Vector3 move = transform.right * x + transform.forward * z;  // pohyb

        characterController.Move(move * player.movementSpeed * Time.deltaTime);

        playerVelocity.y += gravity * Time.deltaTime; // gravitace

        characterController.Move(playerVelocity * Time.deltaTime);

        lastPos = transform.position;  // uložení poslední pozice

        movementSpread = Vector3.Distance(lastPos, pos) * movementSpreadMultiplier;

        if (Vector3.Distance(lastPos, pos) > 0.01f)
        {
            isMoving = Vector3.Distance(lastPos, pos);
        }
        else isMoving = -1f;

    }
}
