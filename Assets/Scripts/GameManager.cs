﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //nastavení hry a editoru
    public bool debugMode;
    public GameObject guns;
    public GameObject player;



    // skripty      
    private PlayerController playerController;
    private GameManager gameManager;
    private Timer timer;
    private Movement movement;


    
    // objekty UI
    [HideInInspector ]public GameObject activeGun;


    private Text gameScore;
    private Text LevelHudText;
    private Text pointsNumberText;
    private Text aimText;
    private Text realoadText;
    private Text runText;
    private Text timeText;
    private Text magsText;
    private GameObject UI;
    private GameObject HUD;
    private GameObject statsPanel;
    private GameObject plusButtons;
    private GameObject PointsAvailaibe;
    private UnityEngine.UI.Slider xpSlider;
    private UnityEngine.UI.Slider xpSliderHUD;

    



    void Start()
    {
        playerController = player.GetComponent<PlayerController>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        timer = GameObject.Find("StartPosition").GetComponent<Timer>();
        movement = GameObject.Find("Player").GetComponent<Movement>();

        UI = GameObject.Find("UI");
        HUD = UI.transform.Find("HUD").gameObject;

        if (UI != null)
        {
            gameScore = HUD.transform.Find("GameScore").GetComponent<Text>();
            statsPanel = UI.transform.Find("StatsPanel").gameObject;
            xpSlider = statsPanel.transform.Find("XPSlider").GetComponent<UnityEngine.UI.Slider>();
            xpSliderHUD = UI.transform.Find("HUD").transform.Find("XPSliderHUD").GetComponent<UnityEngine.UI.Slider>();
            LevelHudText = HUD.transform.Find("LevelHudText").GetComponent<Text>();
            plusButtons = statsPanel.transform.Find("Buttons").gameObject;
            PointsAvailaibe = statsPanel.transform.Find("PointsAvailaible").gameObject;
            pointsNumberText = statsPanel.transform.Find("PointsAvailaible").transform.Find("PointsNumberText").GetComponent<Text>();
            aimText = statsPanel.transform.Find("AimDisplayValue").GetComponent<Text>();
            realoadText = statsPanel.transform.Find("ReloadingDisplayValue").GetComponent<Text>();
            runText = statsPanel.transform.Find("RunningDisplayValue").GetComponent<Text>();
            timeText = HUD.transform.Find("TimeText").GetComponent<Text>();
            magsText = HUD.transform.Find("MagsText").GetComponent<Text>();



            xpSlider.minValue = playerController.lastLevelXP;
            xpSlider.maxValue = playerController.nextLevelXp;

            xpSliderHUD.minValue = playerController.lastLevelXP;
            xpSliderHUD.maxValue = playerController.nextLevelXp;
        }



    }





    // Update is called once per frame
    void Update()
    {
        UpdateHUD();
        UpdateStatsPanel();
        
        
    }






    /// <summary>
    /// UI
    /// </summary>
    void UpdateHUD()
    {
                
        if(HUD.activeInHierarchy)
        {
            xpSlider.minValue = playerController.lastLevelXP;
            xpSlider.maxValue = playerController.nextLevelXp;
            xpSliderHUD.minValue = playerController.lastLevelXP;
            xpSliderHUD.maxValue = playerController.nextLevelXp;

                        
            gameScore.text = playerController.score.ToString();
            timeText.text = timer.min + ":" + timer.sec;
            xpSliderHUD.value = playerController.xp;
            LevelHudText.text = playerController.level.ToString();
            magsText.text = UpdateAmmo();
        }

    }




    void UpdateStatsPanel()
    {
       
        
        if(statsPanel.activeInHierarchy == true)
        {
            xpSlider.value = playerController.xp;
            aimText.text = playerController.aimAbility.ToString();
            realoadText.text = playerController.reloadingAbility.ToString();
            runText.text = playerController.runningAbility.ToString();
            pointsNumberText.text = playerController.skillPoints.ToString();

          
            
            if (playerController.skillPoints >= 1)
            {
                PointsAvailaibe.SetActive(true);
                plusButtons.SetActive(true);
            }
            else
            {
                PointsAvailaibe.SetActive(false);
                plusButtons.SetActive(false);
            }

        }

    }



    /// <summary>
    /// RULES
    /// </summary>
    public void EarlyStart()
    {
        movement.canMove = false; 
        HUD.transform.Find("FaulMessage").gameObject.SetActive(true);
        HUD.transform.Find("FaulMessage").transform.Find("EarlyStartMessage").gameObject.SetActive(true);
                
        StartCoroutine(RestartScene(5));

    }



/// <summary>
/// coroutines
/// </summary>


    private IEnumerator RestartScene(int sec)
    {
        yield return new WaitForSeconds(sec);        

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }



    /// <summary>
    /// gun
    /// </summary>
    
    public string GetActiveGun()
    {
        for (int i = 0; i < guns.transform.childCount; i++)
        {
            if (guns.transform.GetChild(i).gameObject.activeSelf == true)
            {
                activeGun = guns.transform.GetChild(i).gameObject;
            }
        }
        return activeGun.name;
    }




    string UpdateAmmo()
    {
        string gunName = GetActiveGun();

        GameObject activeGun = guns.transform.Find(gunName).gameObject;
        Debug.Log(activeGun.name);

        int ammoInMag = activeGun.GetComponent<Gun>().ammoInMag;
        int mags = playerController.equipedMags;
        string currentAmmo = ammoInMag + "/" + mags;

        return currentAmmo;
    }



}
