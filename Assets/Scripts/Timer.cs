﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public GameObject guns;
    

    [HideInInspector] public GameObject activeGun;
    [HideInInspector] public bool shooterReady = false;
    [HideInInspector] public float runTime;
    [HideInInspector] public string min = "00", sec = "00";
    [HideInInspector] public bool started = false;
    private GameManager gameManager;


    private void Start()
    {
        // guns = GameObject.Find("Player").transform.Find("Fps Camera").transform.Find("FPSHands prefab").transform.Find("CATRigHub001").transform.Find("CATRigRArmCollarbone").transform.Find("CATRigRArm1").
        //     transform.Find("CATRigRArm21").transform.Find("CATRigRArm22").transform.Find("CATRigRArmPalm").transform.Find("Guns").gameObject;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }


    void Update()
    {

        if (shooterReady)
        {
            StartCoroutine(Starter());

        }


        if(started)
        {
            TimeIsRunning();

        }


    }

    private IEnumerator Starter()
    {   
        int randomStart = Random.Range(3, 10);
        yield return new WaitForSeconds(randomStart);

        started = true;

        string gunName = gameManager.GetActiveGun();
        activeGun = GameObject.Find(gunName).gameObject;

        activeGun.GetComponent<Gun>().started = true;
    }

    void TimeIsRunning()
    {
        runTime += Time.deltaTime;
        min = Mathf.Floor((runTime % 3600) / 60).ToString("00");
        sec = (runTime % 60).ToString("00");
       // Debug.Log(min + ":" + sec);

    }
}
