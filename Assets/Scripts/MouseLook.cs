﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class MouseLook : MonoBehaviour
{
    public float sensitivity = 100f; // citlivost myši

    public Transform playerTransform;

    private float xRotation = 0f;
    private Joystick lookJoy;

    void Start()
    {
        lookJoy = GameObject.Find("UI").transform.Find("HUD").transform.Find("Controlls").transform.Find("LookJoystick").GetComponent<FixedJoystick>();

        sensitivity = PlayerPrefs.GetFloat("LookSensitivity");

    }   


    void Update()
    {
        float mouseX = lookJoy.Horizontal * sensitivity * Time.deltaTime;
        float mouseY = lookJoy.Vertical * sensitivity * Time.deltaTime;

       
        //Vertikálně
        xRotation -= mouseY; 
        xRotation = Mathf.Clamp(xRotation, -90f, 60f);

        //Horizontálně
        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        playerTransform.Rotate(Vector3.up * mouseX);

    }
}   
