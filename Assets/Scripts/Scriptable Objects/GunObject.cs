﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


[CreateAssetMenu(fileName = "New Gun", menuName = "Scriptable Gun")]

public class GunObject : ScriptableObject
{
    [Header("Gun Properties")]
    public int ammoInMag = 8;
    public int magCapacity = 8;
    public float gunSpread = 0.03f;
    public float hitForce = 60f;


    [Header("Gun Effects")]
    public ParticleSystem bulletImpactWood;
    public ParticleSystem bulletImpactGround;
    public ParticleSystem bulletImpactSteel;
    public ParticleSystem bulletImpactConcrete;
    public ParticleSystem bulletImpactPaper;


}
